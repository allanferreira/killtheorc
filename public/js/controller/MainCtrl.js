app.controller('MainCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.init = function(){
		$scope.start = true;
		$scope.turn = 0;
		$scope.realtime = {
			battle: {
				currentMessage: ''
			}
		}
		$scope.ruler = {
			char: {
				lifebar: {
					status: {	
						fine: '#ADFF2F',
						tired: '#EFDC05',
						weak: '#FF4C2F'
					}
				}
			},
			message: {
				battle: {
					end: 'Congratulations, you won '
				}
			}
		}
		$scope.hero = {
			name: 'Hero',
			life: {
				status: 'fine',
				current: 100,
				total: 100,
				percent: 100
			},
			atk: 30,
			def: 20,
			mgk: 15,
			xp: {
				current: 150,
				percent: 100,
				total: 600
			}
		}
		$scope.monster = {
			name: 'Monster',
			life: {
				status: 'fine',
				current: 70,
				total: 70,
				percent: 100
			},
			atk: 50,
			def: 10,
			mgk: 0,
			loot: {
				xp: 100
			}
		}
	}
	$scope.init();

	$scope.reset = function(){
		$scope.init();
	}
	
	$scope.changeTurn = function(){
		($scope.turn == 0) ? $scope.turn = 1 : $scope.turn = 0
	}
	
	$scope.endBattle = function(winner,looser){
		$scope.turn = 3;
		$scope.winnerName = winner.name;
		$scope.looserName = winner.name;
	}

	$scope.expBattle = function(from, to){
		var xpCurrent = from.xp.current;
		var xpMonster = to.loot.xp;
		from.xp.current = xpCurrent + xpMonster;
	}

	$scope.helperPercentGen = function(a, b){
		var x = (a*100)/b;
		return x;
	}

	$scope.styleLifeStatus = function(status){
		var lifeStatus = (status == 'fine') ? $scope.ruler.char.lifebar.status.fine : (status == 'tired') ? $scope.ruler.char.lifebar.status.tired : $scope.ruler.char.lifebar.status.weak;
		return lifeStatus;
	}	

	$scope.lifeStatus = function(percentLife, damage, to, from){
		(percentLife > 80) ? to.life.status = "fine" : (percentLife > 20) ? to.life.status = "tired": to.life.status = "weak";
	}

	$scope.styleTurnMessage = function(percentLife, damage, to, from){
		var txt1 = from.name+' inflicts '+damage+' points of damage in '+to.name+'!';
		var txt2 = 'Left over '+percentLife+' percents of life.';
		$scope.realtime.battle.currentMessage = [txt1,txt2];
	}

	$scope.helperMinifyString = function(word, limit){
		var min = word.toString().substring(0, limit);
		return min;
	}

	$scope.atk = function(type, from, to){
		var atk = from.atk;
		var mgk = from.mgk;
		var def = to.def;
		var lifeTotal = to.life.total;
		var life = to.life.current;
		var damage = (type=='mgk') ? mgk: atk-def;
		if(damage > 0){
			var newLife = life-damage;
			if(newLife < 0){
				var newLife = 0;
				$scope.endBattle(from, to);
				$scope.expBattle(from, to);
			}
			else{
				$scope.changeTurn();
			}
		}
		to.life.current = newLife;
		
		var percentLife = $scope.helperPercentGen(to.life.current, to.life.total);
		to.life.percent = $scope.helperMinifyString(percentLife, 2);

		$scope.lifeStatus(to.life.percent, damage, to, from);
		$scope.styleTurnMessage(to.life.percent, damage, to, from);
	}
}]);
