'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var fs = require('fs');

var sendgridkey = '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';
var sendgrid = require('sendgrid')(sendgridkey);

app.use(express.static('public'));

app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
app.use(bodyParser.text({ type: 'text/html' }));

app.get('*', function(req, res) {
    res.status(404).sendfile('./public/index.html');
});

app.get('/', function(req, res) {
    res.sendfile('./public/index.html');
});

app.post('/sendMail', function(req, res) {
	
	var email = new sendgrid.Email();
	var template = (typeof req.body.plan != 'undefined') ? 'email-plano' : 'email-contato'; 
	
	email.addTo('allan.less@outlook.com');
	email.from = 'allan.less@outlook.com';

	email.subject = 'CONTENTHUB site - '+req.body.form;
	email.html = (typeof req.body.plan != 'undefined') ? '<strong>Plano</strong><br>'+req.body.plan : '';
	email.html += '<br><br>';
	email.html += '<strong>Nome</strong><br>'+req.body.name;
	email.html += '<br><br>';
	email.html += '<strong>Email</strong><br>'+req.body.email;
	email.html += '<br><br>';
	email.html += '<strong>Telefone</strong><br>'+req.body.phone;
	email.html += '<br><br>';
	email.html += '<strong>Empresa</strong><br>'+req.body.company;
	email.html += '<br><br>';
	email.html += '<strong>Site</strong><br>'+req.body.domain;
	email.html += '<br><br>';
	email.html += '<strong>Mensagem</strong><br>'+req.body.message;
	email.addCategory('contenthubsite');

	sendgrid.send(email, function(err, json){
		if(err){
			res.status(500).json({err: 'error on sendmail '+err})
		}
		fs.readFile(__dirname+'/public/'+template+'/index.html', 'utf-8', function(err, html){
			if(err){
				console.log('err ', err);
				res.status(500).json({err: 'error on sendmail '+err})
			}

			var email2 = new sendgrid.Email();

			email2.addTo(req.body.email);
			email2.from = 'allan.less@outlook.com';

			email2.subject = 'CONTENTHUB';
			email2.html = html;
			email2.addCategory('retorno-contenthubsite');
			
			sendgrid.send(email2, function(err, json){
				if(err){
					res.status(500).json({err: 'error on sendmail '+err})
				}
				res.end(JSON.stringify(req.body, null, 2));  
			});
		});
	});

});

app.listen(5000);